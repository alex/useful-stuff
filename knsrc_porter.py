import re
import os
from pathlib import Path

def port_cmake_version(project: str, minVersion: str):
    cmakeFile = os.path.join(project, "CMakeLists.txt")
    if os.path.isfile(cmakeFile):
        with open(cmakeFile, mode='r') as f:
            cmakeContent = f.read()
            minMatch = re.search('^set ?\(KF5_MIN_VERSION "([0-9.]+)"+\)', cmakeContent, re.MULTILINE)
            if minMatch and minMatch.group(1) < minVersion:
                newCM = re.sub('set ?\(KF5_MIN_VERSION "([0-9.]+)"+\)', 
                            minMatch.group(0).replace(minMatch.group(1), minVersion), cmakeContent)
                open(cmakeFile, mode='w').write(newCM)
                print("Wrote cmake KF5 version", cmakeFile)


def port_kpackage_explicit(project) -> bool:
    file_list = [f for f in Path(project).resolve().glob('**/*') if f.is_file() and f.name.endswith(".knsrc")]
    ported = False
    if not file_list:
        return ported
    for f in file_list:
        knsrcContent = open(f, mode='r').read()
        match = re.search("""InstallationCommand=kpackagetool5 (?:-t|--type) (\w+/\w+) (?:-i|--install) %f
UninstallCommand=kpackagetool5 (?:-t|--type) (\w+/\w+) (?:-r|--remove) %f""", knsrcContent, re.MULTILINE)
        if not match:
            continue
        if not match.group(1) == match.group(2):
            print("Package file is messed up", f)
            continue
        # Write new values
        lines = []
        for l in knsrcContent.splitlines(keepends=True):
            if l.startswith("InstallationCommand"):
                lines.append("Uncompress=kpackage\n")
                lines.append(f"KPackageType={match.group(1)}\n")
            elif l.startswith("Uncompress") or l.startswith("UninstallCommand"):
                continue
            else:
                lines.append(l)
        open(f, mode='w').writelines(lines)
        print("Wrote knsrc file", f)
        ported = True
    return ported


def port_knsrc_file_location(project) -> bool:
    file_list = [f for f in Path(project).resolve().glob('**/*') if f.is_file() and f.name == "CMakeLists.txt"]
    ported = False
    for cmakeFile in file_list:
        try:
            with open(cmakeFile, mode='r') as f:
                cmakeContentLines = []
                linesChanged = False
                for l in f.readlines():
                    locationMatch = re.search('^install\( *FILES +(.+\.knsrc) +DESTINATION +\${(KDE_INSTALL_CONFDIR)} *\)$', l)
                    if locationMatch:
                        cmakeContentLines.append(re.sub('^install\( *FILES +(.+\.knsrc) +DESTINATION +\${(KDE_INSTALL_CONFDIR)} *\)$',
                                    locationMatch.group(0).replace(locationMatch.group(2), "KDE_INSTALL_KNSRCDIR"), l))
                        ported = True
                        linesChanged = True
                    else:
                        cmakeContentLines.append(l)
                if linesChanged:
                    open(cmakeFile, mode='w').writelines(cmakeContentLines)
                    print("Wrote cmake knsrc file location", cmakeFile)
        except:
            return ported
    return ported


for project in os.listdir(os.path.expanduser('~/kde/src/')):
    project = os.path.expanduser('~/kde/src/') + project
    if not os.path.isdir(project):
        continue
    fileLocationPorted = port_knsrc_file_location(project)

    kpackagePorted = port_kpackage_explicit(project)
    if kpackagePorted:
        port_cmake_version(project, "5.71.0")
    elif fileLocationPorted:
        port_cmake_version(project, "5.57.0")
    if fileLocationPorted or kpackagePorted:
        print("Project was ported", project, "\n")
