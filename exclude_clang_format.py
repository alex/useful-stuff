import os
for project in os.listdir(os.path.expanduser('~/kde/src/frameworks/')):
    if project == "extra-cmake-modules":
        continue
    project = os.path.expanduser('~/kde/src/frameworks/') + project
    # ignore files that are lying around
    if not os.path.isdir(project):
        continue
    # if there is already a clang-format file we do not want to add it to the .gitignore
    should_exclude_clang_format = not os.path.isfile(project + "/.clang-format")
    if os.path.isfile(project + "/.gitignore"):
        with open(project + "/.gitignore") as f:
            for l in f.readlines():
                if ".clang-format" in l:
                    should_exclude_clang_format = False
    if should_exclude_clang_format:
        print(f"Changing .gitignore file in {project}")
        with open(project + "/.gitignore", "a") as f:
            f.write("/.clang-format\n")
            f.close()
            os.system(f"git -C {project} add .gitignore")
            os.system(f"git -C {project} commit -m 'GIT_SILENT Add auto generated .clang-format file to .gitignore'")
            os.system(f"git -C {project} push")
