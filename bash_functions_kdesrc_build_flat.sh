#!/bin/bash

function m {
	if [[ -f "build.ninja" ]];
	then
		ninja install -j$(nproc)
	else
		make install -j$(nproc)
	fi
}

function c { 
	if [[ "$PWD" == "$HOME/kde/build/"* ]];
	then
		cmake "../../src/$(basename $PWD)" -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_CXX_COMPILER=/usr/bin/clazy -DCMAKE_INSTALL_PREFIX=/home/user/kde/usr/ -DBUILD_TESTING=1 -DCMAKE_PREFIX_PATH=/home/user/kde/usr/ $@
	else
		echo "No in a kde build dir"
	fi
}

function s { 
	# Switch to specified source directory
	if [ ! -z "$1" ]
	then
		 cd "$HOME/kde/src/$1"
	else 
		if [[ "$PWD" == "$HOME/kde/build/"* ]];
		then
			# Switch to source directory for current build directory
			cd "$HOME/kde/src/$(echo $PWD | grep -Po "^$HOME/kde/build\K.+" | cut -d "/" -f2)"
		else 
			# Switch to source root
			cd "$HOME/kde/src/"
		fi
	fi
}

function b { 
	# Switch to specified build directory
	if [ ! -z "$1" ]
	then
		 cd "$HOME/kde/build/$1"
	else 
		if [[ "$PWD" == "$HOME/kde/src/"* ]];
		then
			# Switch to build directory for current source directory
			cd "$HOME/kde/build/$(echo $PWD | grep -Po "^$HOME/kde/src\K.+" | cut -d "/" -f2)"
		else 
			# Switch to build root
			cd "$HOME/kde/build/"
		fi
	fi
}

_kde_complete_src()
{
	local cur prev opts
	COMPREPLY=()
	cur="${COMP_WORDS[COMP_CWORD]}"
	prev="${COMP_WORDS[COMP_CWORD-1]}"
	opts="$(ls ~/kde/src)"

	COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
	return 0
}
_kde_complete_build()
{
	local cur prev opts
	COMPREPLY=()
	cur="${COMP_WORDS[COMP_CWORD]}"
	prev="${COMP_WORDS[COMP_CWORD-1]}"
	opts="$(ls ~/kde/build)"

	COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
	return 0
}
complete -F _kde_complete_src s
complete -F _kde_complete_build b
