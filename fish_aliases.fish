#!/usr/bin/fish

alias cls="clear;clear"
export EDITOR=vim
# export LLVM_ROOT=(llvm-config --prefix)
export CC=/usr/bin/clang
export CXX=/usr/bin/clazy
export EDITOR=nvim
export CLAZY_CHECKS="level0,no-qenums,no-const-signal-or-slot,no-fully-qualified-moc-types,no-qproperty-without-notify,no-overloaded-signal,no-connect-not-normalized,no-qstring-ref,no-strict-iterators"
export CMAKE_EXPORT_COMPILE_COMMANDS=ON
alias kde=". ~/.config/kde-env-master.sh"

alias rg="/usr/bin/rg -g '!*.po'"

function unset
  set --erase $argv
end

set VIM (which vim)
function vim
    if string match  -q -r 'file://.+:.+' $argv
        $VIM (echo $argv | string split 'file://' | tail -1 | string split :| head -1) +0(echo $argv | string split : |head -3 | tail -1)
    else if string match  -q -r 'file://' $argv
        $VIM (echo $argv | string split 'file://' | tail -1 | string split :| head -1)
    else if string match  -q -r ':' $argv
        $VIM (echo $argv | string split :| head -1) +0(echo $argv | string split : |head -2 | tail -1)
    else
        $VIM $argv
    end
end
