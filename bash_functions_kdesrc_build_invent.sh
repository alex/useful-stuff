#!/bin/bash

function m {
	if [[ -f "build.ninja" ]];
	then
		ninja install -j$(nproc)
	else
		make install -j$(nproc)
	fi
}

function c { 
	if [[ "$PWD" == "$HOME/kde/build/"* ]];
	then
		cmake "~/kde/src/$(echo $PWD | sed 's/\/home\/user\/kde\/build//g')" -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_INSTALL_PREFIX=/home/user/kde/usr/ -DBUILD_TESTING=1 -DCMAKE_PREFIX_PATH=/home/user/kde/usr/ $@
	else
		echo "No in a kde build dir"
	fi
}

function s { 
	# Switch to specified source directory
	if [ ! -z "$1" ]
	then
		 cd $(find  ~/kde/src/  -mindepth 2 -maxdepth 2 -type d |grep "/$1\$")
	else 
		if [[ "$PWD" == "$HOME/kde/build/"* ]];
		then
			# Switch to source directory for current build directory
			cd "$HOME/kde/src/$(echo $PWD | grep -Po "^$HOME/kde/build\K.+")"
		else 
			# Switch to source root
			cd "$HOME/kde/src/"
		fi
	fi
}

function b { 
	# Switch to specified build directory
	if [ ! -z "$1" ]
	then
		 cd $(find  ~/kde/build/  -mindepth 2 -maxdepth 2 -type d |grep "/$1\$")
	else 
		if [[ "$PWD" == "$HOME/kde/src/"* ]];
		then
			# Switch to build directory for current source directory
			cd "$HOME/kde/build/$(echo $PWD | grep -Po "^$HOME/kde/src\K.+")"
		else 
			# Switch to build root
			cd "$HOME/kde/build/"
		fi
	fi
}

_kde_complete_src()
{
	local cur prev opts
	COMPREPLY=()
	cur="${COMP_WORDS[COMP_CWORD]}"
	prev="${COMP_WORDS[COMP_CWORD-1]}"
	opts="$(find  ~/kde/src/  -mindepth 2 -maxdepth 2 -type d  | sed 's/\/home\/user\/kde\/src\/.*\///g')"

	COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
	return 0
}
_kde_complete_build()
{
	local cur prev opts
	COMPREPLY=()
	cur="${COMP_WORDS[COMP_CWORD]}"
	prev="${COMP_WORDS[COMP_CWORD-1]}"
	opts="$(find  ~/kde/build/  -mindepth 2 -maxdepth 2 -type d  | sed 's/\/home\/user\/kde\/build\/.*\///g')"

	COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
	return 0
}
complete -F _kde_complete_src s
complete -F _kde_complete_build b
