#!/bin/bash

alias cls="clear;clear"
export EDITOR=vim
export CC=/usr/bin/clang
export CXX=clazy
export EDITOR=vim
export CLAZY_CHECKS="level0,no-qenums,no-const-signal-or-slot,no-fully-qualified-moc-types,no-qproperty-without-notify,no-overloaded-signal,no-connect-not-normalized,no-qstring-ref"
alias kde=". ~/.config/kde-env-master.sh"

VIM=$(which vim)
function vim {
    local args
    IFS=':' read -a args <<< "$1"
    "$VIM" "${args[0]}" +0"${args[1]}"
}

