#!/usr/bin/fish

function m
	if test -f "build.ninja";
		ninja install -j(nproc)
	else
		make install -j(nproc)
	end
end

function c
	set qt_prefix (grep qtdir ~/.config/kdesrc-buildrc|sed -n -e 's/^ *qtdir *//p')
	set kde_prefix (grep kdedir ~/.config/kdesrc-buildrc|sed -n -e 's/^ *kdedir *//p')
	set installdir_new (grep install-dir ~/.config/kdesrc-buildrc|sed -n -e 's/^ *install-dir *//p')
	set prefix_path "$installdir_new"
	if test -n "$kde_prefix"
		set prefix_path "$prefix_path;$kde_prefix"
	end
	if test -n "$qt_prefix"
		set prefix_path "$prefix_path;$qt_prefix"
	end
	set cmake_options (grep '^ *cmake-options' ~/.config/kdesrc-buildrc | sed -r 's/ *cmake-options *//' | head -1)
	set generator_option (grep '^ *cmake-generator' ~/.config/kdesrc-buildrc | sed -r 's/ *cmake-generator *//' | head -1)
	if test -n "$generator_option"
    set generator_option "-G $generator_option"
	end
	if string match "$PWD" == "$HOME/kde/build/"*
		echo cmake "$HOME/kde/src/"(basename $PWD) $generator_option -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_INSTALL_PREFIX=/home/user/kde/usr/ -DBUILD_TESTING=1 -DCMAKE_PREFIX_PATH=\'$prefix_path\' $cmake_options $argv
		eval cmake "$HOME/kde/src/"(basename $PWD) $generator_option -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_INSTALL_PREFIX=/home/user/kde/usr/ -DBUILD_TESTING=1 -DCMAKE_PREFIX_PATH=\"$prefix_path\" $cmake_options $argv
	else
		echo "No in a kde build dir"
	end
end


function s
	if count $argv > /dev/null
		# Switch to specified source directory
		if test -d "$HOME/kde/src/"$argv[1]
			# Switch to source directory for current build directory
			cd "$HOME/kde/src/"$argv[1]
		else
			echo "No source directory found"
		end
	else if test -d "$HOME/kde/build/"(basename $PWD)
		# Switch to source dir of current build dir
		cd "$HOME/kde/src/"(basename $PWD)
	else
		# Switch to source dir of all projects
		cd "$HOME/kde/src/"
	end
end

function b
	if count $argv > /dev/null
		# Switch to specified build directory
		if test -d "$HOME/kde/build/"$argv[1]
			# Switch to build directory for current source directory
			cd "$HOME/kde/build/"$argv[1]
		else
			echo "No build directory found"
		end
	else if string match -q "$HOME/kde/src/*" "$PWD" 
			# Switch to build dir of current build dir
			cd (string replace $HOME/kde/src/ $HOME/kde/build/ -- $PWD)
	else
		# Switch to build dir of all projects
		cd "$HOME/kde/build/"
	end
end

complete --no-files -c c -a "-DBUILD_TESTING=1 -DEXCLUDE_DEPRECATED_BEFORE_AND_AT=0 -DEXCLUDE_DEPRECATED_BEFORE_AND_AT=CURRENT"
complete --no-files -c s -a "(ls ~/kde/src/)"
complete --no-files -c b -a "(ls ~/kde/build/)"
